module github.com/champly/lib4go

go 1.15

require (
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/champly/gojenkins v1.0.0
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1
	github.com/docker/go-connections v0.4.0
	github.com/docker/go-units v0.4.0 // indirect
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/go-zookeeper/zk v1.0.2
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.4.2
	github.com/mailru/easyjson v0.7.2 // indirect
	github.com/olivere/elastic v6.2.34+incompatible
	github.com/onsi/ginkgo v1.14.0
	github.com/onsi/gomega v1.10.1
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/pkg/sftp v1.11.0
	github.com/tealeg/xlsx v1.0.5
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
	golang.org/x/text v0.3.3
	k8s.io/klog/v2 v2.3.0
	mosn.io/pkg v0.0.0-20200729115159-2bd74f20be0f
)
